//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Stefan Eletzhofer on 16.05.16.
//  Copyright © 2016 Stefan Eletzhofer. All rights reserved.
//

import Foundation

class CalculatorBrain {
    
    // public interface
   
    var result : Double {
        get { return accumulator }
    }
    
    var isPartialResult : Bool {
        get { return pendingOperation != nil }
    }
    
    var description = ""
    
    func clearAll() {
        print("CalculatorBrain: clearAll")
        accumulator = 0.0
        pendingOperation = nil
        description = ""
        internalProgram.removeAll()
    }
    
    func setOperand(op:Double) {
        accumulator = op
        internalProgram.append(op)
        description += "\(accumulator) "
    }
    
    func performOperation(symbol:String) {
        if let op = operations[symbol] {
            internalProgram.append(symbol)
            switch op {
            case .Constant(let value):
                accumulator = value
                description += "\(symbol) "
            case .UnaryOp(let function):
                description = "\(symbol)(\(description))"
                accumulator = function(accumulator)
            case .BinaryOp(let function):
                description += "\(symbol) "
                if let pending = pendingOperation {
                    performPendingOperation(pending)
                }
                pendingOperation = PendingOperation(operand: accumulator, symbol: symbol, function: function)
            case .Equal:
                if let pending = pendingOperation {
                    performPendingOperation(pending)
                }
            }
        } else {
            print("Unknown operation: \(symbol)")
        }
    }
    
    typealias PropertyList = AnyObject
    var program : PropertyList {
        get {
            return internalProgram
        }
        set {
            if let arrayOfOps = newValue as? [AnyObject] {
                clearAll()
                for op in arrayOfOps {
                    if let operand = op as? Double {
                        setOperand(operand)
                    }
                    if let symbol = op as? String {
                        performOperation(symbol)
                    }
                }
            }
        }
    }
    
    // private
    
    private func performPendingOperation(pending : PendingOperation) {
        accumulator = pending.function(pending.operand, accumulator)
        pendingOperation = nil
    }
    
    private struct PendingOperation {
        var operand : Double
        var symbol : String
        var function: (Double, Double) -> Double
    }
    
    
    private var pendingOperation : PendingOperation? = nil
    
    private enum Operation {
        case Constant(Double)
        case UnaryOp((Double) -> Double)
        case BinaryOp((Double, Double) -> Double)
        case Equal
    }
    
    private var operations : Dictionary<String,Operation> = [
        "π": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        
        "±": Operation.UnaryOp({-$0}),
        "x⁻¹": Operation.UnaryOp({1.0/$0}),
        "x²": Operation.UnaryOp({$0 * $0}),
        "√": Operation.UnaryOp(sqrt),
        "cos": Operation.UnaryOp(cos),
        "sin": Operation.UnaryOp(sin),
        "tan": Operation.UnaryOp(tan),
        "sinh": Operation.UnaryOp(sinh),
        "cosh": Operation.UnaryOp(cosh),
        "log": Operation.UnaryOp(log10),
        "ln": Operation.UnaryOp(log),
        "eˣ": Operation.UnaryOp(exp),
        
        "+": Operation.BinaryOp({$0 + $1}),
        "−": Operation.BinaryOp({$0 - $1}),
        "×": Operation.BinaryOp({$0 * $1}),
        "÷": Operation.BinaryOp({$0 / $1}),
        
        "yˣ": Operation.BinaryOp({pow($0, $1)}),
        
        "=": Operation.Equal
    ]
    
    private var accumulator = 0.0
    
    
    private var internalProgram = [AnyObject]()
}
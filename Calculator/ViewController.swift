//
//  ViewController.swift
//  Calculator
//
//  Created by Stefan Eletzhofer on 16.05.16.
//  Copyright © 2016 Stefan Eletzhofer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let brain = CalculatorBrain()
    private var userIsTypingANumber = false;
    private var program : CalculatorBrain.PropertyList?
    
    private var displayValue : Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    private var resultDescription : String {
        get {
            if let text = resultDescriptionLabel.text {
                return text
            } else {
                return ""
            }
        }
        set {
            resultDescriptionLabel.text = newValue
        }
    }
    
    private func updateCalcResult() {
        if brain.isPartialResult || userIsTypingANumber {
            resultDescription = brain.description + " ..."
        } else {
            resultDescription = brain.description + " ="
        }
    }
    
    // outlets
    @IBOutlet private weak var display: UILabel!
    @IBOutlet private weak var resultDescriptionLabel: UILabel!
    
    
    // actions
    @IBAction private func digitTouched(sender: UIButton) {
        
        let theDigitText = sender.currentTitle!
        let theDisplayText = display.text!
        
        if theDigitText == "." && theDisplayText.rangeOfString(".") != nil {
            return
        }
    
        if userIsTypingANumber {
            display.text = theDisplayText + theDigitText
        } else {
            userIsTypingANumber = true
            if theDigitText == "." {
                display.text = "0" + theDigitText
            } else {
                display.text = theDigitText
            }
        }
        updateCalcResult()
    }
    
    @IBAction private func operationPressed(sender: UIButton) {
        let theOperation = sender.currentTitle!
        // print("operationPressed: \(theOperation)")
        
        if userIsTypingANumber {
            brain.setOperand(displayValue)
            userIsTypingANumber = false
        }
        
        brain.performOperation(theOperation)
        
        displayValue = brain.result
        updateCalcResult()
    }
    
    @IBAction private func saveTouched() {
        program = brain.program
    }
    
    
    @IBAction private func loadTouched() {
        if let prog = program {
            brain.program = prog
            displayValue = brain.result
            updateCalcResult()
        }
    }
    
    @IBAction private func clrTouched() {
        program = nil
    }
    
    @IBAction private func clearButtonPressed(sender: UIButton) {
        brain.clearAll()
        displayValue = brain.result;
        userIsTypingANumber = false
        resultDescription = ""
        updateCalcResult()
    }
}

